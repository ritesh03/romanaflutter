import 'dart:convert';
import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:romanadriver/config/constants.dart';
import 'package:http/http.dart';
import 'package:romanadriver/screens/base/base_screen.dart';
import 'package:romanadriver/screens/drawerScreen/navigation_drawer.dart';
import 'package:romanadriver/screens/passenger%20detail/passenger_details.dart';
import 'package:romanadriver/screens/trips/trip_list_model.dart';
import 'package:romanadriver/utils/gernal_methods.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

String TAG = "Trips.dart";

class Trips extends StatefulWidget {
  Trips({Key? key}) : super(key: key);

  static const String routeName = 'Trips';
  static Route route() {
    return MaterialPageRoute(
      builder: (_) => Trips(),
      settings: const RouteSettings(name: routeName),
    );
  }

  @override
  _TripsState createState() => _TripsState();
}

class _TripsState extends BaseScreen with WidgetsBindingObserver {
  String drivername = "";
  String driveremail = "";
  DateTime currentDate = DateTime.now();
  String formattedDate = "";
  String showDate = "";
  String day = "";
  int tripStatus = 0;
  String version = "";
  DateTime? currentBackPressTime;

  String getDay() {
    var dayArray = [
      Constants.monday,
      Constants.tuesday,
      Constants.wednesday,
      Constants.thursday,
      Constants.friday,
      Constants.saturday,
      Constants.sunday,
    ];
    var dow = currentDate.weekday;
    if (day == "") {
      print(TAG + "current day ${dayArray[dow - 1]}");
      return dayArray[dow - 1];
    } else {
      return day;
    }
  }

  Future<List<TripData>> _getPosts() async {

      formattedDate = DateFormat('yyyy-MM-dd').format(currentDate);

      print("$TAG formatted date ${formattedDate}");

      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('accessToken') ?? "";
      final driverid = prefs.getString('driverid') ?? "";

      try{
    Response res = await get(
      Uri.parse(
          '${Constants.baseUrl}admin/trip/triplist?uniquieAppKey=${Constants.unique_key}'
              '&driverId=${driverid}&startDate=${formattedDate}&tripStatus=$tripStatus'),
      headers: {
        'Authorization': 'bearer ${token}',
        'Content-Type': 'application/json'
      },
    );
    print(res.statusCode);
    if (res.statusCode == 200) {
      print(TAG + "response body trrp ${json.decode(res.body)}");
      Trip_list_model triplistmodel =
          Trip_list_model.fromJson(json.decode(res.body));
      // print(TAG+ " trip model data ${triplistmodel.data?.tripData?[0].id}");
      List<TripData>? posts = triplistmodel.data?.tripData!;

      return posts!;
    } else {
      throw "Unable to retrieve posts.";
    }
      }
      catch(e){
        GernalMethods().showFaliureApiDialog(context, Constants.CONNECTION_ERROR);
        throw "$TAG trips faliure :-- ${e}";
      }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));

    if (pickedDate != null && pickedDate != currentDate) {
      setState(() {
        print("$TAG piocked date ${pickedDate}");
        currentDate = pickedDate;
        _getPosts();
      });
    }
  }


  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Press again to exit");
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);

  }


  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("APP_STATE: $state");
    if (state == AppLifecycleState.resumed) {
      currentDate = DateTime.now();
      _getPosts();
    } else if (state == AppLifecycleState.inactive) {
    } else if (state == AppLifecycleState.paused) {}
  }

  @override
  Widget build(BuildContext context) {
    print("$TAG drivername in prefs widget $drivername");
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).primaryColor,
            statusBarIconBrightness: Brightness.light,
          ),
          title: Text(Constants.tripstitle,
              style: Theme.of(context)
                  .textTheme
                  .headline1
                  ?.copyWith(color: Colors.white, fontWeight: FontWeight.w400)
          ),
          centerTitle: true,
          elevation: 0,
        ),
        drawer: NavigationDrawer(),
        body: Column(
          children: [
            Container(
              height: 40,
              color: Theme.of(context).bottomAppBarColor,
              child: Row(
                children: <Widget>[
                  IconButton(
                      onPressed: () {
                        setState(() {
                          _getPosts();
                        });
                      },
                      icon: const Icon(Icons.refresh)),
                  Expanded(
                      child: Center(
                          child:TextButton(child: Text('${DateFormat('dd/MM/yyyy').format(currentDate)} ${getDay()}',
                              style: const TextStyle(fontSize: 16, color: Colors.black)),
                          onPressed: () => _selectDate(context),)
                      )
                  ),
                  IconButton(
                      onPressed: () => _selectDate(context),
                      icon: const Icon(Icons.calendar_today_rounded))
                ],
              ),
            ),
            Container(
                child: handleTripsApiResonse(context))
          ],
        ),
      ),
    );
  }
  Widget handleTripsApiResonse(BuildContext context){
    return Container(
        child: Center(
          child: FutureBuilder(
              future:  _getPosts(),
              builder: (context, AsyncSnapshot<List<TripData>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  final List<TripData>? triplist = snapshot.data;
                  if(triplist!.isEmpty){
                    return _noPostFoundText(context);
                  }else {
                    return _buildPosts(context, triplist);
                  }
                } else if (snapshot.hasError) {
                  print(TAG + " error snapshot ${snapshot.error}");
                   return _noPostFoundText(context);
                }
                return SizedBox(
                  height: MediaQuery.of(context).size.height * 0.80,
                  child: const Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                );
              }),
        )
    );
  }
}

Future clearprefs() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.clear();
}


Widget _noPostFoundText(BuildContext context){
  return  SizedBox(
    height: MediaQuery.of(context).size.height * 0.80,
    child: Padding(
      padding: const EdgeInsets.all(5.0),
      child: Center(
        child: Text("No trips found", style: Theme.of(context).textTheme.headline1?.copyWith(fontWeight: FontWeight.w500)),
      ),
    ),
  );
}

Widget _buildPosts(BuildContext context, List<TripData> posts) {
  // print("$TAG list print ${posts[0].id} ");
  String getTripRequest(int tripr) {
    switch (tripr) {
      case 0:
        return "Pick Up";
      case 1:
        return "Drop Off";
      case 2:
        return "Will Call";
    }
    return "undefined ";
  }


    return ListView.builder(
        physics: const ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: posts.length,
        padding: const EdgeInsets.all(8),
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [Icon(Icons.directions_car, color: Colors.black)],
                ),
                title: Text(
                  '${posts[index].tripTime.toString()} ${getTripRequest(int.parse(posts[index].tripRequest!))} ${posts[index].passengerName?.firstName} ',
                ),
                subtitle: Text('${posts[index].destinationAddress?.streetAddress} ${posts[index].destinationAddress?.city} '
                    '${posts[index].destinationAddress?.country} ${posts[index].destinationAddress?.postalCode}',
                   overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Passenger(passengerDetail: posts[index])));
                }),
          );
        });

}
