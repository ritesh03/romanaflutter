import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:romanadriver/config/constants.dart';
import 'package:romanadriver/screens/base/base_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:romanadriver/screens/login/login_model.dart';
import 'package:romanadriver/screens/trips/trips.dart';
import '../forgot_password.dart';

bool responseApi = false;
String TAG = "Login";

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  static const String routeName = 'Login';
  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const Login(),
      settings: const RouteSettings(name: routeName),
    );
  }

  @override
  _LoginState createState() => _LoginState();
}
class _LoginState  extends BaseScreen {

  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool _isHidden = true;

  Future<Login_model> performLogin(BuildContext ctx, String username, String password ) async {
    print(TAG+" username $username , password $password");
    showProgress();
    try {
      final response = await http.post(
        Uri.parse('${Constants.baseUrl}driver/login'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "email": username,
          "password": password,
          "uniquieAppKey": "a873db9ec1764cfa32b66962e92bf5bd"
        }),
      );
      final responseJson = jsonDecode(response.body) as Map<String, dynamic>;
      String msg = responseJson['message'].toString();
      print(response.statusCode);
      if (response.statusCode == 200) {
        var loginmodel = Login_model.fromJson(json.decode(response.body));
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('accessToken', loginmodel.data.accessToken);
        prefs.setString('driverid', loginmodel.data.data.id);
        prefs.setString('drivername',
            loginmodel.data.data.name + " " + loginmodel.data.data.lastName);
        prefs.setString('driveremail', loginmodel.data.data.email);
        print(TAG + "pref access token  ${prefs.getString('accessToken')}");
        print(TAG + " status code :-- ${response.body}");
        hideProgress();
        Navigator.push(
            ctx,
            MaterialPageRoute(
                builder: (context) => Trips())
        );
      }
      else if (response.statusCode == 400) {
        hideProgress();
        showDialog(context: ctx,
            builder: (ctx) =>
                AlertDialog(
                  content: Text(msg),
                  actions: [
                    TextButton(child: const Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                  ],
                )
        );
      }
      return Login_model.fromJson(jsonDecode(response.body));
    }
    catch(e){
      hideProgress();
     showDialog(context: context,
          builder: (ctx) => AlertDialog(
            content:  Text(Constants.CONNECTION_ERROR),
            actions: [
              TextButton(child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
      );
      throw "$TAG login faliure :-- ${e}";
    }
  }


  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Theme.of(context).primaryColor,
          statusBarIconBrightness: Brightness.light,
        ),
        centerTitle: true,
        elevation: 0,
        title: Text( Constants.logintitle, style: Theme.of(context).textTheme.headline1
            ?.copyWith(color: Colors.white, fontWeight: FontWeight.w400))
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                    padding: const EdgeInsets.all(15.0),
                    child: Image.asset('assets/logoromana.png',)
                ),
                Container(
                  padding: const EdgeInsets.all(15.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: Constants.emaillabeltext,
                      hintText: Constants.emailhinttext,
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Enter your email Id';
                      } else if (!(value.contains("@"))){
                        return  'Enter a valid email Id';
                      }
                    },
                    controller: emailController,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: TextFormField(
                    obscureText: _isHidden,
                    controller: passwordController,
                    decoration: InputDecoration(
                      labelText: Constants.passwordlabeltext,
                      hintText: Constants.passwordhinttext,
                      border: const OutlineInputBorder(),
                      suffixIcon: InkWell(
                        onTap: _togglePasswordView,
                        child: Icon(
                          _isHidden ? Icons.visibility_off : Icons.visibility,
                        ),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Enter your password';
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                          child: Text( Constants.forgotbutton ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    const ForgotPassword()));
                          })),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ElevatedButton(
                      child: Text( Constants.loginbutton , style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.white)),
                      style: ElevatedButton.styleFrom(primary: Color(0xFF359EC1)),
                      onPressed: () {
                        if(_formKey.currentState!.validate()){
                          performLogin(context, emailController.text, passwordController.text);
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


}




