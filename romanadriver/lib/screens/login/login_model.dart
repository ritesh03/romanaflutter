class Login_model {
  Login_model({
      required int statusCode,
      required String message,
      required Data data,}){
    _statusCode = statusCode;
    _message = message;
    _data = data;
}

  Login_model.fromJson(dynamic json) {
    _statusCode = json['statusCode'];
    _message = json['message'];
    _data = (json['data'] != null ? Data.fromJson(json['data']) : null)!;
  }
  late int _statusCode;
  late String _message;
  late Data _data;

  int get statusCode => _statusCode;
  String get message => _message;
  Data get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = _statusCode;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.toJson();
    }
    return map;
  }

}

class Data {
  Data({
      required String accessToken,
      required Data datadata,}){
    _accessToken = accessToken;
    _dataData = _dataData;
}

  Data.fromJson(dynamic json) {
    _accessToken = json['access_token'];
    _dataData = (json['data'] != null ? DataData.fromJson(json['data']) : null)!;
  }
  late String _accessToken;
  late DataData _dataData;

  String get accessToken => _accessToken;
  DataData get data => _dataData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = _accessToken;
    if (_dataData != null) {
      map['data'] = _dataData.toJson();
    }
    return map;
  }

}

class DataData {
  DataData({
      required String id,
      required String name,
      required String lastName,
      required String email,
      required String type,}){
    _id = id;
    _name = name;
    _lastName = lastName;
    _email = email;
    _type = type;
}

  DataData.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['Name'];
    _lastName = json['lastName'];
    _email = json['email'];
    _type = json['type'];
  }
  late String _id;
  late String _name;
  late String _lastName;
  late String _email;
  late String _type;

  String get id => _id;
  String get name => _name;
  String get lastName => _lastName;
  String get email => _email;
  String get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['Name'] = _name;
    map['lastName'] = _lastName;
    map['email'] = _email;
    map['type'] = _type;
    return map;
  }

}