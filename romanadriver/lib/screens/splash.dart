import 'dart:async';
import 'package:flutter/material.dart';
import 'package:romanadriver/screens/drawerScreen/navigation_drawer.dart';
import 'package:romanadriver/screens/trips/trips.dart';
import 'login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

String TAG = "Splash.dart";

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  static const String routeName = 'Splash';

  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const Splash(),
      settings: const RouteSettings(name: routeName),
    );
  }

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {


  @override
  void initState() {
    super.initState();
  }

  Future<String> getTokenFromPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('accessToken') ?? "";
    if (token == "") {

      return "";
    } else {

      return token;
    }
  }



  @override
  Widget build(BuildContext context) {
    return Container(
    child: FutureBuilder(
    future: getTokenFromPrefs(),
    builder: (context, AsyncSnapshot<String> snapshot) {
    if (snapshot.connectionState == ConnectionState.done &&
    snapshot.hasData){
    print("$TAG , access token value:-- ${snapshot.data}");

      if(snapshot.data == ""){
        Timer(
            const Duration(seconds: 3),
                () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => const Login())));
      }
      else{
        Timer(
            const Duration(seconds: 3),
                () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) =>  Trips())));
      }
    }
    else if(snapshot.hasError){
    print("$TAG , access token value:-- ${snapshot.error}");
    }
    return Container(
    decoration: const BoxDecoration(
          color: Colors.white
            ),
            child: Center(
    child: Image.asset(
    'assets/logoromana.png',
    width: 300,
    ),
    ),
    );
    },

    )
    ,
    );

  }
}