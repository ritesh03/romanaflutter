import 'package:flutter/material.dart';

 abstract class BaseScreen<T extends StatefulWidget> extends State<T> {
  bool _loaderShowing = false;


  @protected
  showProgress() {
    _loaderShowing = true;
    showDialog(
        context: context,
        builder: (builder) => const Center(
            child: SizedBox(
              child: CircularProgressIndicator(
                valueColor:
                AlwaysStoppedAnimation<Color>(Color(0xFF359EC1)),
              ),
              width: 45,
              height: 45,
            ),
          ),
        useRootNavigator: false,
        barrierDismissible: false)
        .then((value) {
      _loaderShowing = false;
    });
  }

  @protected
  void hideProgress() {
    if (_loaderShowing) {
      Navigator.of(context).pop();
    }
  }

}
