import 'dart:convert';
import 'dart:ui';
import 'package:romanadriver/config/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:romanadriver/screens/base/base_screen.dart';
import 'package:romanadriver/screens/passenger%20detail/status_update.dart';
import 'package:romanadriver/screens/trips/trip_list_model.dart';
import 'package:romanadriver/screens/trips/trips.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'notes_model.dart';
import 'package:maps_launcher/maps_launcher.dart';

String TAG = "Passenger Details";

class Passenger extends StatefulWidget {
  Passenger({Key? key, required this.passengerDetail}) : super(key: key);
  TripData passengerDetail;


  static const String routeName = 'Passenger';

  static Route route() {
    return MaterialPageRoute(
      builder: (_) => Passenger(passengerDetail: TripData()),
      settings:  const RouteSettings(name: routeName),
    );
  }

  @override
  _PassengerState createState() => _PassengerState(tripData: passengerDetail);
}

class _PassengerState extends BaseScreen {
  int checktripStatus = 0;
  String notes="";
  String comments="";
  String submitButtonData = "";
  String address = "";
  String phoneNo= "";

  final notesController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getTripStatusString();
    print("$TAG check initstate ");
    address = '${tripData.originAddress?.streetAddress} ${tripData.originAddress?.city} ${tripData.originAddress?.country} ${tripData.originAddress?.postalCode}';
    if(tripData.notes!.isNotEmpty){
      notes = tripData.notes![tripData.notes!.length-1].note!;
    }
    if(tripData.originAddress!.phoneNo!= "") {
      phoneNo = tripData.originAddress!.phoneNo!;
    }

  }

  Future<String> notesApiCall(String note) async {
    showProgress();
    final prefs = await SharedPreferences.getInstance();
    final driverid = prefs.getString('driverid') ?? "";

    try{
    final response = await http.post(Uri.parse('${Constants.baseUrl}admin/driver/create_Notes'),
    headers: <String, String>{
      'Authorization': 'bearer ${Constants.Admin_access_token}',
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
    "uniquieAppKey":Constants.unique_key,
    "driverId":driverid,
    "tripId":tripData.id!,
    "note": note,
     })
    );
    print("$TAG response check ${response.body}");
    if(response.statusCode == 200){
      Notes_model noteresponse = Notes_model.fromJson(jsonDecode(response.body));
     notes = noteresponse.data!.data!.notes![noteresponse.data!.data!.notes!.length-1].note!;
     hideProgress();
     setState(() {
     });
      return notes;
    }else {
      hideProgress();
      throw "Unable to retrieve notes.";
    }}
    catch(e){
      hideProgress();
      showDialog(context: context,
          builder: (ctx) => AlertDialog(
            content:  Text(Constants.CONNECTION_ERROR),
            actions: [
              TextButton(child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
      );
    //  GernalMethods.showFaliureApiDialog(context, "Connection error! Please try again");
      throw "$TAG notes faliure :-- ${e}";
    }
  }

  String getTripStatusString(){
    print("$TAG ${tripData.tripStatus}");
    switch(tripData.tripStatus){
      case 1:
        submitButtonData = "On Route";
        checktripStatus = 1;
        address ='${tripData.originAddress?.streetAddress} ${tripData.originAddress?.city} ${tripData.originAddress?.country} ${tripData.originAddress?.postalCode}';
        comments = "${tripData.originAddress?.comments}";
        if(tripData.originAddress!.phoneNo!= "") {
          phoneNo = tripData.originAddress!.phoneNo!;
        }
        return "On Route";
      case 2:
        submitButtonData = "Picked Up";
        checktripStatus = 2;
        address ='${tripData.originAddress?.streetAddress} ${tripData.originAddress?.city} ${tripData.originAddress?.country} ${tripData.originAddress?.postalCode}';
        comments = "${tripData.originAddress?.comments}";
        if(tripData.originAddress!.phoneNo!= "") {
          phoneNo = tripData.originAddress!.phoneNo!;
        }
        return "Picked Up";
      case 3:
        submitButtonData = "Delivered";
        checktripStatus = 3;
        address ='${tripData.destinationAddress?.streetAddress} ${tripData.destinationAddress?.city} ${tripData.destinationAddress?.country} ${tripData.destinationAddress?.postalCode}';
        comments = "${tripData.destinationAddress?.comments}";
        if(tripData.destinationAddress!.phoneNo!= "") {
          phoneNo = tripData.destinationAddress!.phoneNo!;
        }
        return "Delivered";
    }
    print("$TAG get trip status string check submit button data  $submitButtonData");
    print("$TAG get trip status string check check trip status $checktripStatus");
    return "";
  }

  Future routedetail(int status) async {
    final prefs = await SharedPreferences.getInstance();
    final driverid = prefs.getString('driverid') ?? "";

    showProgress();
    try {
      final routeresponse = await http.put(
          Uri.parse('http://52.8.254.207:8004/trip/update'),
          headers: <String, String>{
            'Authorization': 'bearer ${Constants.Admin_access_token}',
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            "uniquieAppKey": Constants.unique_key,
            "driverName": driverid,
            "id": tripData.id!,
            "tripStatus": status.toString(),
          })
      );
      print("$TAG route response status check ${routeresponse.statusCode}");
      print("$TAG route response body check ${routeresponse.body}");

      if (routeresponse.statusCode == 200) {
        hideProgress();
        print("$TAG route response successfull");
        Status_update updatedstatus = Status_update.fromJson(
            jsonDecode(routeresponse.body));
        checktripStatus = updatedstatus.data!.tripStatus!;
        print("$TAG tripStatus:-- ${checktripStatus}");
        setState(() {
          switch (checktripStatus) {
            case 1:
              checktripStatus = 1;
              submitButtonData = "On Route";
              if(tripData.originAddress!.phoneNo!= "") {
                phoneNo = tripData.originAddress!.phoneNo!;
              }
              break;
            case 2:
              checktripStatus = 2;
              submitButtonData = "Picked Up";
              if(tripData.originAddress!.phoneNo!= "") {
                phoneNo = tripData.originAddress!.phoneNo!;
              }
              break;
            case 3:
              checktripStatus = 3;
              submitButtonData = "Delivered";
              address = '${tripData.destinationAddress?.streetAddress} ${tripData
                  .destinationAddress?.city} ${tripData.destinationAddress
                  ?.country} ${tripData.destinationAddress?.postalCode}';
              comments = "${tripData.destinationAddress?.comments}";
              if(tripData.destinationAddress!.phoneNo!= "") {
                phoneNo = tripData.destinationAddress!.phoneNo!;
              }
              break;
            case 4:
              checktripStatus = 4;
              submitButtonData = "Delivered";
              showDialog(context: context,
                  builder: (ctx) =>
                      AlertDialog(
                        content: const Text("Thank you for the trip"),
                        actions: [
                          TextButton(child: const Text('OK'),
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Trips()),
                                      (Route<dynamic> route) => false,
                                );
                              }),
                        ],
                      )
              );
              break;
          }
          print("$TAG tripStatus after update:-- $checktripStatus");
        });
      } else {
        throw "Route response error";
      }
    }
    catch(e){
      hideProgress();
      showDialog(context: context,
          builder: (ctx) => AlertDialog(
            content:  Text(Constants.CONNECTION_ERROR),
            actions: [
              TextButton(child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
      );
      throw "$TAG trips status faliure :-- ${e}";
    }
  }



  @override
  Widget build(BuildContext context) {
    notesController.text = notes;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).primaryColor,
            statusBarIconBrightness: Brightness.light,
          ),
          leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: () {
            Navigator.pop(context);
          } ),
          elevation: 0,
          centerTitle: true,
          title: Text('${tripData.tripTime} ${getTripRequest(int.parse(tripData.tripRequest!))}',
              style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.white, fontWeight: FontWeight.w400))
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
               if( tripData.confirmationNumber != "")
                 Padding(
                   padding: const EdgeInsets.fromLTRB(15, 0, 15,0),
                   child: Text('Confirmation Number : ${tripData.confirmationNumber}',
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black)),
                 ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                child: Text(Constants.passengerdetailtile, style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black)),
              ),
              Row(
                children: [
                   Expanded(child: Padding(
                     padding: const EdgeInsets.fromLTRB(15, 12, 15, 0),
                     child: Text('${tripData.passengerName?.firstName} ${tripData.passengerName?.lastName}', style: Theme.of(context).textTheme.bodyText1),
                   )),
                  if(phoneNo != "")
                    IconButton( icon: const Icon(Icons.call), onPressed: () => launch('tel://${phoneNo}'))
                  //IconButton( icon: const Icon(Icons.call), onPressed: () => launch("tel://21213123123"))
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                child: Text(Constants.addressdetailtile, style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black)),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.navigation_rounded),
                      onPressed: () {
                        MapsLauncher.launchQuery(address);
                      }
                    ),
                    Expanded(child: Text(address, style: Theme.of(context).textTheme.bodyText1))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                child: Text(Constants.notestitle, style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black)),
              ),
              if( comments != "")
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                child: Text(comments, style: Theme.of(context).textTheme.bodyText1),
              ) else
                  Padding(
                  padding: const EdgeInsets.fromLTRB(15, 12, 15,0),
                  child: Text('No notes', style: Theme.of(context).textTheme.bodyText1),
                 ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text("Custom Notes", style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black)),
                      ),
                    ),
                    if(notes != "")
                    IconButton(icon: const Icon(Icons.create_outlined),
                      onPressed: () {
                      notesController.text = notes;
                        showDialog(context: context,
                            builder: (ctx) => AlertDialog(
                              title: const Center(child: Text('Add Custom Note')),
                              content: TextField(
                                maxLines: 20,
                                controller: notesController,
                                decoration: InputDecoration(
                                  fillColor: Colors.grey[300],
                                  filled: true,
                                ),
                              ),
                              actions: [
                                ElevatedButton(child: const Text('Cancel'),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                                ElevatedButton(child: const Text('Save'),
                                    onPressed: () {
                                      notesApiCall(notesController.text);
                                      Navigator.pop(context);
                                    }),
                              ],
                            )
                        );
                      },
                    )else
                      TextButton(child: const Text('Add Notes'),
                        onPressed: () {
                          showDialog(context: context,
                              builder: (ctx) => AlertDialog(
                                title: const Center(child: Text('Add Custom Note')),
                                content: TextField(

                                  maxLines: 20,
                                  controller: notesController,
                                  decoration: InputDecoration(
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                  ),
                                ),
                                actions: [
                                  ElevatedButton(child: const Text('Cancel'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      }),
                                  ElevatedButton(child: const Text('Save'),
                                      onPressed: () {
                                        notesApiCall(notesController.text);
                                        Navigator.pop(context);
                                      }),
                                ],
                              )
                          );
                        },
                      )
                 ]
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Text(notes, style: Theme.of(context).textTheme.bodyText1),
                ),
              )
            ]
          ),
        ),
      ),
        bottomNavigationBar: Container(
            height: 50,
            width: double.infinity,
            margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: ElevatedButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.directions_car),
                  SizedBox(width: 5),
                  Text(submitButtonData),
                ],
              ),
              onPressed: () {
                setState(() {
                  switch(checktripStatus) {
                    case 0:
                    //routedetail();
                      break;
                    case 1:
                      routedetail(Constants.DRIVER_ON_ROUTE);
                      break;
                    case 2:
                      routedetail(Constants.PICKEDUP);
                      break;
                    case 3:
                      routedetail(Constants.DELIVERED);
                      break;
                    case 4:
                    //routedetail(status);
                      break;
                    case 5:
                    //routedetail(status);
                      break;
                    case 6:
                    //routedetail(status);
                      break;
                    case 7:
                    //routedetail(status);
                      break;
                    case 8:
                    //routedetail(status);
                      break;
                  }
                });
              },
            ),
          ),

    );
  }

  _PassengerState({
    required this.tripData
  });

  TripData tripData;
    String getTripRequest(int tripr) {
      switch (tripr) {
        case 0:
          return "Pick Up";
        case 1:
          return "Drop Off";
        case 2:
          return "Will Call";
      }
      return "undefined ";
    }

}
