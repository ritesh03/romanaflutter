import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:romanadriver/config/constants.dart';
import 'package:http/http.dart' as http;
import 'package:romanadriver/screens/base/base_screen.dart';
import 'package:romanadriver/screens/drawerScreen/navigation_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login/login.dart';

String TAG = "Change password";

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  static const String routeName = 'ChangePassword';
  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const ChangePassword(),
      settings: const RouteSettings(name: routeName),
    );
  }
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}
class _ChangePasswordState extends BaseScreen {
  final _formKey = GlobalKey<FormState>();
  final currentpasswordController = TextEditingController();
  final newpasswordController = TextEditingController();
  final confirmpasswordController = TextEditingController();
  bool _isHiddenCurrent = true;
  bool _isHiddenNew = true;
  bool _isHiddenConfirm = true;
  void _toggleCurrentPasswordView() {
    setState(() {
      _isHiddenCurrent = !_isHiddenCurrent;
    });
  }

  void _toggleNewPasswordView() {
    setState(() {
      _isHiddenNew = !_isHiddenNew;
    });
  }

  void _toggleConfirmPasswordView() {
    setState(() {
      _isHiddenConfirm = !_isHiddenConfirm;
    });
  }

  Future changepassword(BuildContext ctx, String currentpassword, String newpassword) async {
    showProgress();
    final prefs = await SharedPreferences.getInstance();
    final driverid = prefs.getString('driverid') ?? "";
    try {
      final response = await http.put(
        Uri.parse(
            '${Constants.baseUrl}driver/updatePassword?uniquieAppKey=${Constants
                .unique_key}'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "uniquieAppKey": Constants.unique_key,
          "id": driverid,
          "oldPassword": currentpassword,
          "password": newpassword
        }),
      );
      final responseJson = jsonDecode(response.body) as Map<String, dynamic>;
      String msg = responseJson['message'].toString();

      print("$TAG ${response.statusCode}");
      if (response.statusCode == 200) {
        hideProgress();
        showDialog(
            context: ctx,
            builder: (ctx) =>
                AlertDialog(
                  content: const Text("Your password is changed successfully."),
                  actions: [
                    TextButton(
                        child: const Text('OK'),
                        onPressed: () {
                         // clearprefs();
                          Navigator.pop(context);
                        }),
                  ],
                ));
        currentpasswordController.clear();
        newpasswordController.clear();
        confirmpasswordController.clear();
      }
      else if (response.statusCode == 401) {
        hideProgress();
        showDialog(
            context: ctx,
            builder: (ctx) =>
                AlertDialog(
                  content: const Text("Session Expired"),
                  actions: [
                    TextButton(
                        child: const Text('OK'),
                        onPressed: () {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Login()),
                                (Route<dynamic> route) => false,
                          );
                        }),
                  ],
                ));
      } else if (response.statusCode == 400) {
        hideProgress();
        SnackBarClosedReason.hide;
        showDialog(
            context: ctx,
            builder: (ctx) =>
                AlertDialog(
                  content: Text(msg),
                  actions: [
                    TextButton(
                        child: const Text('OK'),
                        onPressed: () {
                         // clearprefs();
                          Navigator.pop(context);
                        }),
                  ],
                ));
      }
    }
    catch(e){
      hideProgress();
      showDialog(context: context,
          builder: (ctx) => AlertDialog(
            content:  Text(Constants.CONNECTION_ERROR),
            actions: [
              TextButton(child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
      );
      throw "$TAG change password faliure :-- $e";
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).primaryColor,
            statusBarIconBrightness: Brightness.light,
          ),
          centerTitle: true,
          elevation: 0,
          title: Text(Constants.changescreentitle,
              style: Theme.of(context).textTheme.headline1?.copyWith(
                  color: Colors.white, fontWeight: FontWeight.w400))),
      drawer: NavigationDrawer(),
      body: GestureDetector(
          child: SingleChildScrollView(
            child: SafeArea(
              minimum: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextFormField(
                        obscureText: _isHiddenCurrent,
                        controller: currentpasswordController,
                        decoration: InputDecoration(
                          labelText: Constants.currentpasswordlabel,
                          hintText: Constants.currentpasswordhinttext,
                          border: const OutlineInputBorder(),
                          suffixIcon: InkWell(
                            onTap: _toggleCurrentPasswordView,
                            child: Icon(
                              _isHiddenCurrent
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter your password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: TextFormField(
                        obscureText: _isHiddenNew,
                        controller: newpasswordController,
                        decoration: InputDecoration(
                          labelText: Constants.changepasswordlabel,
                          hintText: Constants.newpasswordhinttext,
                          border: const OutlineInputBorder(),
                          suffixIcon: InkWell(
                            onTap: _toggleNewPasswordView,
                            child: Icon(
                              _isHiddenNew
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter your password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextFormField(
                        obscureText: _isHiddenConfirm,
                        controller: confirmpasswordController,
                        decoration: InputDecoration(
                          labelText: Constants.confirmpasswordlabel,
                          hintText: Constants.newpasswordhinttext,
                          border: const OutlineInputBorder(),
                          suffixIcon: InkWell(
                            onTap: _toggleConfirmPasswordView,
                            child: Icon(
                              _isHiddenConfirm
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter your password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: ElevatedButton(
                          child: Text(Constants.changepasswordbtn,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(color: Colors.white)),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              if (newpasswordController.text !=
                                  confirmpasswordController.text) {
                                showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                          content: const Text(
                                              "New password and Confirm password must be same"),
                                          actions: [
                                            TextButton(
                                                child: const Text('OK'),
                                                onPressed: () {
                                                //  clearprefs();
                                                  Navigator.pop(context);
                                                }),
                                          ],
                                        ));
                              } else {
                                changepassword(
                                  context,
                                  currentpasswordController.text,
                                  newpasswordController.text,
                                );
                              }
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(FocusScopeNode());
          }),
    );
  }
}
