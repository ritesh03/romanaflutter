import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:romanadriver/config/constants.dart';
import 'package:http/http.dart' as http;
import 'package:romanadriver/screens/base/base_screen.dart';
import 'login/login.dart';

String TAG = "forgot_password";

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  static const String routeName = 'ForgotPassword' ;
  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const ForgotPassword(),
      settings: const RouteSettings(name: routeName),
    );
  }

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends BaseScreen {

  final emailcontroller = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Future forgotpassword(String email) async {
    showProgress();
    try {
      final response = await http.post(
        Uri.parse('${Constants.baseUrl}driver/verify'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "uniquieAppKey": Constants.unique_key,
          "email": email
        }),
      );
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      String msg = responseJson['message'].toString();
      print("$TAG response msg $msg");
      print("$TAG ${response.statusCode}");
      if (response.statusCode == 200) {
        hideProgress();
        showDialog(context: context,
            builder: (ctx) =>
                AlertDialog(
                  content: Text("Check your emailId"),
                  actions: [
                    TextButton(child: const Text('OK'),
                        onPressed: () {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Login()),
                                (Route<dynamic> route) => false,
                          );
                        }),
                  ],
                )
        );
      }
      else if (response.statusCode == 400) {
        hideProgress();
        showDialog(context: context,
            builder: (ctx) =>
                AlertDialog(
                  content: Text(msg),
                  actions: [
                    TextButton(child: const Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                  ],
                )
        );
      }
    }
    catch(e){
      hideProgress();
      showDialog(context: context,
          builder: (ctx) => AlertDialog(
            content:  Text(Constants.CONNECTION_ERROR),
            actions: [
              TextButton(child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
      );
      throw "$TAG Forgot password faliure :--- $e";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).primaryColor,
            statusBarIconBrightness: Brightness.light,
          ),
          leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: () {
            Navigator.pop(context);
          } ),
          centerTitle: true,
          elevation: 0,
          title: Text(Constants.forgotscreentitle,
              style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.white, fontWeight: FontWeight.w400))
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            minimum: const EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: Text('Enter your Email Id below to reset your password',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(15, 24, 15, 0),
                    child: TextFormField(
                      controller: emailcontroller,
                        decoration: InputDecoration(
                          labelText: Constants.emaillabeltext,
                          border: OutlineInputBorder(),
                          hintText: Constants.emailhinttext,
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                          return 'Enter your email Id';
                          } else if (!(value.contains("@"))){
                          return  'Enter a valid email Id';
                          }
                          return null;
                          },
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 75,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(15, 24, 15, 0),
                      child: ElevatedButton(
                          child: Text(Constants.forgotpasswordscreenbutton, style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.white) ),
                          onPressed: () {
                            if(_formKey.currentState!.validate()) {
                              forgotpassword(emailcontroller.text);
                            }
                          }
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
    );
  }

}
