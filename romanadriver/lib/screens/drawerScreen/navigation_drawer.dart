import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:romanadriver/config/constants.dart';
import 'package:romanadriver/screens/base/base_screen.dart';
import 'package:romanadriver/screens/login/login.dart';
import 'package:romanadriver/screens/trips/trips.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info_plus/package_info_plus.dart';

class NavigationDrawer extends StatefulWidget{
  @override
  _DrawerState createState() => _DrawerState();
}

class _DrawerState extends BaseScreen{


  String drivername = "";
  String driveremail = "";
  String TAG = "NavigationDrawer";


  Future<String> getAppVersion() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
   String  version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    return version;
  }


  Future<void> driverinfo() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      drivername = prefs.getString('drivername') ?? "";
      print("$TAG drivername in prefs $drivername");
      driveremail = prefs.getString('driveremail') ?? "";
    });
  }
  @override
  Widget build(BuildContext context) {
    driverinfo();
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(context,drivername,driveremail),
          createDrawerBodyItem(
            icon: Icons.home,
            text: 'Home',
            onTap: () =>
                Navigator.pushReplacementNamed(context, "Trips"),
          ),
          createDrawerBodyItem(
            icon: Icons.security,
            text: 'Change Password',
            onTap: () =>
                Navigator.pushReplacementNamed(context, "ChangePassword"),
          ),
          createDrawerBodyItem(
            icon: Icons.logout,
            text: 'Log Out',
            onTap: () {
              showDialog(
                  context: context,
                  builder: (ctx) => AlertDialog(
                    content: Text(Constants.signout),
                    actions: <Widget>[
                      TextButton(
                          child: const Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      TextButton(
                          child: const Text('OK'),
                          onPressed: () {
                            clearprefs();
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Login()),
                                  (Route<dynamic> route) => false,
                            );
                          }),
                    ],
                  ));
            },

          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.40,
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: FutureBuilder(
                future: getAppVersion(),
                builder: (context, AsyncSnapshot<String> snapshot) {
                  if(snapshot.hasData){
                    print("$TAG version :-- ${snapshot.data}");
                    return Text("version ${snapshot.data}");
                  }
                  else{
                    print("$TAG version snapshot error :-- ${snapshot.error}");
                    return const Text("");
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }


  Widget createDrawerHeader(BuildContext context,String drivername, String driveremail) {
    return DrawerHeader(
      decoration: const BoxDecoration(color: Color(0xFF359EC1)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text(drivername,
              style: Theme.of(context)
                  .textTheme
                  .headline1
                  ?.copyWith(color: Colors.white, fontWeight: FontWeight.w400),

              textAlign: TextAlign.left,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Text(
                driveremail,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(color: Colors.white),
                textAlign: TextAlign.left),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 16),
            child: Text('Romana Medical Transportation',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(color: Colors.white),
                textAlign: TextAlign.left),
          )
        ],
      ),
    );
  }

  Widget createDrawerBodyItem(
      {required IconData icon, required String text, required GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }

}

