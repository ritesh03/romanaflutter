import 'package:flutter/material.dart';
import 'package:romanadriver/screens/calendar.dart';
import 'package:romanadriver/screens/change_password.dart';
import 'package:romanadriver/screens/forgot_password.dart';
import 'package:romanadriver/screens/login/login.dart';
import 'package:romanadriver/screens/passenger%20detail/passenger_details.dart';
import 'package:romanadriver/screens/splash.dart';
import 'package:romanadriver/screens/trips/trips.dart';

class AppRouter{
  static Route onGenerateRoute( RouteSettings settings) {
    print('The Route is: ${settings.name}');
    switch (settings.name){
      case'/':
        return Splash.route();
      case Splash.routeName:
        return Splash.route();
      case Login.routeName:
        return Login.route();
      case Trips.routeName:
        return Trips.route();
      case ForgotPassword.routeName:
        return ForgotPassword.route();
      case ChangePassword.routeName:
        return ChangePassword.route();
      case Calendar.routeName:
        return Calendar.route();
      case Passenger.routeName:
        return Passenger.route();
      default:
        return _errorRoute();
    }
  }
  static Route _errorRoute() {
    return MaterialPageRoute(
        builder: (_) => const Scaffold(body: Text('Error Screen')),
        settings: const RouteSettings(name: '/error')
    );
  }
}