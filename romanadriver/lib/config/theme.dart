
import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
    primaryColor: const Color(0xFF359EC1),
    bottomAppBarColor: const Color(0x4D808080),
    textTheme: const TextTheme(
      headline1: TextStyle(
          fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
      bodyText1: TextStyle(
       fontSize: 16
      ),
    ),
  );
}